import java.awt.Color;

class Mountain extends Cell{
	//fields
	
	//constructors
	public Mountain(int x, int y) {
		super(x, y, Color.LIGHT_GRAY, "Mountain", 10);
	}
}
