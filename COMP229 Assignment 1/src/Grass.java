import java.awt.Color;

class Grass extends Cell{
	//fields
	
	//constructors
	public Grass(int x, int y, int r, int g, int b) {        
		super(x, y, new Color(r, g, b), "Grass", (g-100)/50);
	}
}