import java.awt.*;
import java.util.Random;

class Grid {
    //fields
    Cell[][] cells = new Cell[20][20];

    // constructor
    public Grid(){
    	Random rand = new Random();
        for(int i = 0; i < cells.length; i++){
            for(int j = 0; j < cells[i].length; j++){
            	int type = rand.nextInt(120);
            	if (type < 10) {
            		cells[i][j] = new Mountain(10+35*i, 10+35*j);
            	} else if (type >= 10 && type < 20) {
            		cells[i][j] = new Road(10+35*i,10+35*j);
            	} else if (type >= 20 && type < 30) {
            		cells[i][j] = new Water(10+35*i,10+35*j);
            	} else {
            		cells[i][j] = new Grass(10+35*i,10+35*j, rand.nextInt(50)+1, 100 + rand.nextInt(155) + 1, rand.nextInt(50)+1);
            	}
            }
        }
    }

    // methods
    public void paint(Graphics g, Point mousePos){
        for(int i = 0; i < cells.length; i++){
            for(int j = 0; j < cells[i].length; j++){
                cells[i][j].paint(g, mousePos);
            }
        }
    }
}