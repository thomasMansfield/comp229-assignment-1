import java.awt.*;

class Cell extends java.awt.Rectangle{
    //fields
    static int size = 35;
    int movement;
    String type;
    Color colour;

    //constructors
    public Cell(int x, int y, Color c, String s, int m){
        super(x, y, size, size);
        type = s;
        movement = m;
        colour = c;
    }

    //methods
    void paint(Graphics g, Point p){
        g.setColor(colour);
        g.fillRect(this.x,this.y,size,size);
        g.setColor(Color.BLACK);
        g.drawRect(this.x,this.y,size,size);
        
        if (contains(p)) {
        	
        }
    }

    @Override
    public boolean contains(Point p){
        if (p == null){
            return false;
        } else {
            return super.contains(p);
        }
    }
}